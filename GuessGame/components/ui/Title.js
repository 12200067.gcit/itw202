import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Colors from '../../constants/colors'
const Title = ({children}) => {
  return (
    <View>
      <Text style = {styles.ti}>{children}</Text>
    </View>
  )
}

export default Title

const styles = StyleSheet.create({
    ti:{
        borderWidth: 3,
        borderColor: Colors.white,
        textAlign: 'center',
        color: Colors.white,
        fontSize: 24,
        fontWeight: 'bold',
        padding:10,
    },
})