import  React from 'react'
import {Text, View, Image, StyleSheet} from 'react-native'
import PrimaryButton from '../components/ui/PrimaryButton'
import Title from '../components/ui/Title'
import Colors from '../constants/colors'

function GameOverScreen({roundsNumber, userNumber, onStartNewGame}){
    return(
        <View style={styles.rootContainer} >
            <Title>GAME OVER !</Title>
            <View style={styles.imageContainer} >
            <Image
            style={styles.image}
            source={require('../assets/images/success.png')}/>
            </View>
            <Text style={styles.summaryText} >
                Your phone needed <Text style={styles.highlight} >X </Text>
                rounds to guess the number <Text style={styles.highlight}>Y</Text>
            </Text>
            <PrimaryButton onPress={onStartNewGame}>Start New Game</PrimaryButton>
        </View>
    )
}

export default GameOverScreen

const styles = StyleSheet.create({
    rootContainer:{
        flex: 1,
        padding: 24,
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageContainer:{
        width: 300,
        height: 300,
        borderRadius: 150,
        borderWidth: 3,
        borderColor: Colors.primary800,
        overflow: 'hidden',
        margin: 36,
    },
    image:{
        height: '100%',
        width: '100%',
    },
    summaryText:{
        fontSize: 24,
        textAlign: 'center',
        marginBottom:24,
        fontFamily: 'open-sans',
    },
    highlight:{
        color: Colors.primary500,
    },
})