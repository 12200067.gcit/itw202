import { StatusBar } from "expo-status-bar";
import { View, StyleSheet } from "react-native";
import ImageComponent from "./components/imageComponent.js";
export default function App() {
  return (
    <View style={styles.container}>
      <ImageComponent></ImageComponent>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
