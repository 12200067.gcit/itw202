import React from "react";
import { StyleSheet, View, Image } from "react-native";

const ImageComponent = () => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.picture1}
        source={require("../assets/reactnative.png")}
      />
      <Image
        style={styles.picture2}
        source={{
          uri: "https://picsum.photos/100/100",
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItem: "center",
  },
  picture1: {
    width: 100,
    height: 100,
    resizeMode: "contain",
  },
  picture2: {
    width: 100,
    height: 100,
    resizeMode: "contain",
  },
});

export default ImageComponent;
