import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import MyComponent from './component/MyComponent.js'; 

export default function App() {
  return (
    <View style={styles.container}>
      <MyComponent></MyComponent> 
      <StatusBar style="auto" />
    </View>  
  );
}

const styles = StyleSheet.create({
  container: { 
      fontSize: 24,
      justifyContent: 'center',
      textAlign: 'center',
      alignItems: 'center', 
      flex: 1,

  }, 
});