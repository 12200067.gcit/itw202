import React from "react";
import {Text, StyleSheet, View} from 'react-native';

const MyComponent = () =>{
    return ( 
        <View>
            <Text style = {styles.textStyle}>This is the demo for JSX </Text>
            <Text>we can assemble different Jsx elements like normal HTMl</Text> 
        </View> 
    )
};
export default MyComponent; 

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 24,
        alignItems: 'center', 
        justifyContent: 'center', 
    },  
  }); 

