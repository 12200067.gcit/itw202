import React, {useState} from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';

const ButtonComponent =() => {
  const [count, setCount] = useState(1);
  const [text, setText] = useState("The button isn't pressed yet");

    return ( 
      <View style={styles.container}>
      <Button 
      title='PRESS ME' 
      disabled={count>3} 
      onPress={()=> setCount(count +1)
       setText('The button is pressed'+ count + 'times')}
      /> 
      </View> 
    
  );
};
const styles = StyleSheet.create({
  container: { 
    flex: 1,
    justifyContent: "center",
  },
  fortext: {
    justifyContent: "center",
   
  },
});

export default ButtonComponent;
