import React from 'react';
import { Text, View, Button, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

function Screen1({ navigation }) {
  // console.log(navigation)
  return (
    <View style={styles.container}>
      <Text>Screen 1</Text>
      <Button 
      title ="Go to screen screen"
      onPress={()=> {
        navigation.navigate('Screen 2')
      }}
      />  
    </View>
  );
}
function Screen2({ navigation }) { 
  return (
    <View style={styles.container}>
      <Text>Screen 2</Text>
      <Button
        title="Go to third screen"
        onPress={() => {
          navigation.replace('Screen 3');
        }}
      />
    </View>
  );
}
function Screen3({navigation}) {
  return (
    <View style={styles.container}>
      <Text>Screen 3</Text>
      <Button
      title='Go back'
      onPress={()=> { 
        navigation.goBack();
      }}
      />
    </View>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Screen1"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Screen 1" component={Screen1} />
        <Stack.Screen name="Screen 2" component={Screen2} />
        <Stack.Screen name="Screen 3" component={Screen3} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
});
