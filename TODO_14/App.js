import React from "react";
import { Text, View, Image, StyleSheet } from "react-native";
import { Provider } from "react-native-paper";
import { theme } from "./src/core/theme";
// import Button from './src/components/Button';
// import TextInput from './src/components/TextInput';
// import Header from './src/components/Header';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
  StartScreen,
  LoginScreen,
  RegisterScreen,
  ResetPasswordScreen,
  HomeScreen,
  ProfileScreen,
} from "./src/Screens";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="StartScreen"
          screenOptions={{ headerShown: false }}
        >
          <Stack.Screen name="StartScreen" component={StartScreen} />
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
          <Stack.Screen
            name="ResetPasswordScreen"
            component={ResetPasswordScreen}
          />
          <Stack.Screen name="HomeScreen" component={BottomNavigation} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

function BottomNavigation() {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarIcon: ({ size }) => {
            return (
              <Image
                style={{ Width: size, height: 10 }}
                source={require("./assets/home.jpg")}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({ size }) => {
            return (
              <Image
                style={{ width: size, height: size }}
                source={require("./assets/setting-icon.png")}
              />
            );
          },
        }}
      />
    </Tab.Navigator>
  );
}
