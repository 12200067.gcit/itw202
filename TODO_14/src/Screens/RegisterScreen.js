import React, {useState} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native'
import Header from '../components/Header';
// import Paragraph from '../components/Paragraph';
import Button from '../components/Button';
import Background from '../components/Background';
import Logo from '../components/Logo'; 
import TextInput from '../components/TextInput';
import { emailValidator } from '../core/helpers/emailValidator'; 
import { passwordValidator } from '../core/helpers/passwordValidator';
import BackButton from '../components/BackButton';
import {nameValidator} from '../core/helpers/nameValidator';
import {theme } from '../core/theme';

export default function RegisterScreen({navigation}){ 
  const [email, setEmail] = useState({value: '', error: ''})
  const [password, setPassword] = useState({value: '', error: ''})
  const [name, setName] = useState({value: '', error: ''})

  const onSignUpPressed = () => {
    const emailError = emailValidator(email.value); 
    const passwordError = passwordValidator(password.value);
    const nameError = nameValidator(name.value)
    if (emailError || passwordError || nameError){
      setName({...name, error: nameError});
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
    }
  }  
  return( 
    <Background>
    <BackButton goBack = {navigation.goBack}/>
    <Logo />
      <Header>Create Account </Header>
      <TextInput label ='Name' 
      value = {name.value}
      error = {name.error}
      errorText = {name.error}
      onChangeText = {(text) =>setName({value: text, error: ""})} 
      />
      <TextInput label ='Email' 
      value = {email.value}
      error = {email.error}
      errorText = {email.error}
      onChangeText = {(text) =>setEmail({value: text, error: ""})} 
      />  
       <TextInput label ='Password' 
      value = {password.value}
      error = {password.error} 
      onChangeText = {(text) =>setPassword({value: text, error: ""})} 
      secureTextEntry
      /> 
      <Button mode = 'contained' onPress = {onSignUpPressed}>Sign up</Button>
      <View style = {styles.row}>
      <Text>Already have an account?</Text>
      <TouchableOpacity
      onPress = {() => navigation.replace ('LoginScreen')}>
      <Text style = {styles.link}> Login</Text>
      </TouchableOpacity>
      </View>
   </Background> 
  )
}
const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
})
