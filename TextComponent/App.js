import { StatusBar } from "expo-status-bar";
import { Text, View } from "react-native";
import styles from "./components/textComponent.js";

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.textnormal}>
        The <Text style={styles.textbold}>quick brown fox </Text>
        jumps over the lazy dog
      </Text>
    </View>
  );
}
