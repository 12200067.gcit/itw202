import React from "react";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffff",
    alignItems: "flex-start",
    justifyContent: "center",
  },
  textnormal: {
    fontSize: 16,
  },
  textbold: {
    fontSize: 16,
    fontWeight: "bold",
  },
});
export default styles;
