import { StatusBar } from "expo-status-bar";
import { View, StyleSheet } from "react-native";
import TextInputComponent from "./components/textInput.js";

export default function App() {
  return (
    <View style={styles.container}>
      <TextInputComponent></TextInputComponent>
    </View>
  );
}
const styles = StyleSheet.create({ 
  container: {
    justifyContent: "center",
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
  },
});
