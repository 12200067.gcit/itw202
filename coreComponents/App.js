import { StatusBar } from "expo-status-bar";
import { View } from "react-native";
import styles from "./components/viewComponent.js";

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.box1}></View>
      <View style={styles.box2}></View>
    </View>
  );
}
