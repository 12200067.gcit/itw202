import React from "react";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 300,
    marginBottom: 200,
  },
  box1: {
    backgroundColor: "red",
    flex: 2,
    height: 30,
    width: 125,
  },
  box2: {
    backgroundColor: "blue",
    flex: 2,
    height: 30,
    width: 125,
  },
});
export default styles;
