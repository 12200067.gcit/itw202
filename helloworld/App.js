import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import {add, multiply} from './components/nameExport';
import division from './components/defaultExport';
import Courses from './components/funcComponent';
import React from 'react';


// functions
// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Hello world</Text>
//       <Text>Result of Addition: {add(5, 6)}</Text>
//       <Text>Result of Multiplication: {multiply(5, 8)}</Text>
//       <Text>Result of Division: {division(20, 2)}</Text> 
//       <StatusBar style="auto" />
//     </View>
//   );
// }

// Class components 
// There cannot be two default exports in a module
// export default class App extends React.Component{
//   render = () => (
//     <View style={styles.container}>
//       <Text>Practical 2 </Text>
//       <Text>Stateless and Statefull components</Text>
//       <Text style={styles.text}>
//         You are ready to start the journey. 
//       </Text>
//     </View>
    
//   )

// } 
export default function App() {
  return (
    <View style={styles.container}> 
    <Text></Text>
      <Courses></Courses>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
