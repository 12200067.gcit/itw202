import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import styles from './component/styles.js';
export default function App() {
  return (
    <View style={styles.container}>
     <View style={styles.box1}><Text>1</Text></View>
     <View style={styles.box2}><Text>2</Text></View>
     <View style={styles.box3}><Text>3</Text></View>
    </View>     
  );
}