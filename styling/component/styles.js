import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        marginTop: 80,
        marginRight: 100,
    },
    box1: {
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        backgroundColor: 'red',
        width: 30,
        height: 150,
        borderWidth: 1,
        borderColor: 'black',
    },
    box2: {
        backgroundColor: 'blue',
        height: 150,
        width: 70,
        borderWidth: 1,
        borderColor: 'black',
        justifyContent: 'center',
        textAlign: 'center',
        alignItems: 'center',
    },
    box3: {
        backgroundColor: 'green',
        height: 150,
        width: 13,
        borderWidth: 1,
        borderColor: 'black',
        justifyContent: 'center',
        textAlign: 'center',
    },
});
export default styles;